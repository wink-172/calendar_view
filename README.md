天天跳舞第三方控件统一部署  极大的节省编译时间   在2021-10-13版本编译时间从大于1m到20s以下，最高速度10s
把腾讯信鸽屏蔽，编译速度达到4s(信鸽需要联网检查配置信息且返回时间不稳定，只需要在调试信鸽打开就行)
日历控件+识别动作控件+svga控件
移动mode.json也可提高编译速度
assets文件 drawable文件（无依赖） mipmapwen文件 
所有固定的代码文件都放到这里
此时编译速度最高可达到2s

#### 介绍


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

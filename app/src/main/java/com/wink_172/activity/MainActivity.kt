package com.wink_172.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.wink_172.commons.R
import com.wink_172.commons.databinding.ActivityMainBinding

class MainActivity : Activity() {
    var binding: ActivityMainBinding? = null
    companion object {
        fun startActivity(context: Context, vararg args: Any?) {
            val intent = Intent(context, MainActivity::class.java)

            //intent.putExtra(Constants.PARAM_DATA1, (String) args[0]);
            context.startActivity(intent)
        }

        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    }


}
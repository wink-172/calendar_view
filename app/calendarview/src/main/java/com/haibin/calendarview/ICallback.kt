package com.haibin.calendarview

import java.io.Serializable

/**
 * 全局callback
 *Serializable 可用于intent传递
 * */
open interface ICallback :Serializable {
    fun onSendEvent(event: Int,vararg args: Any?)
}

/**
 * 使用方法1
 * **/
//    val callback=object:
//       com.haibin.calendarview.ICallback {
//        override fun onSendEvent(event: Int, vararg args: Any?) {//这个callback用的非常巧妙//   这里因为不能传递非Serialiazable,对象 所以需要精简代码
//            when (event) {
//
//            }
//        }
//    }

/**
 * 使用方法2
 *
 * */
//    object :
//        com.haibin.calendarview.ICallback {
//        override fun onSendEvent(event: Int,vararg args: Any?) {//这个callback用的非常巧妙    //这里因为不能传递非Serialiazable,对象 所以需要精简代码
//
//        }
//    }
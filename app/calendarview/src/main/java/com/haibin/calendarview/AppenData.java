package com.haibin.calendarview;

import java.io.Serializable;

/**
 * 追加的数据集中
 * */
public class AppenData implements Serializable {


    int score=0;
    private int whole_color_emoji;  //全颜色emoji 0无 1全金色 2全蓝色 3全紫色


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    public int getWhole_color_emoji() {
        return whole_color_emoji;
    }

    public void setWhole_color_emoji(int whole_color_emoji) {
        this.whole_color_emoji = whole_color_emoji;
    }
}
